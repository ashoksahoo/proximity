let express = require('express');
let router = express.Router();

// Require user controller.
let userController = require('../controllers/userController');

// POST request for login page.
router.post('/login', userController.login);

// GET request for logout page.
router.get('/logout', userController.logout);

// POST request for create User.
router.post('/register', userController.register);


module.exports = router;
