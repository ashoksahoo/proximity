let express = require('express');
let router = express.Router();


// Require our controllers.
let video_controller = require('../controllers/videoController');
let tag_controller = require('../controllers/tagsController');
let course_controller = require('../controllers/courseController');
let subject_controller = require('../controllers/subjectController');
let lesson_controller = require('../controllers/lessonController');


/// TAG ROUTES ///

// POST request for creating Tag.
router.post('/tag/create', tag_controller.tag_create);
// POST request to delete Tag.
router.delete('/tag/:id', tag_controller.tag_delete);
// POST request to update Tag.
router.post('/tag/:id/update', tag_controller.tag_update);
// GET request for one Tag.
router.get('/tag/:id', tag_controller.tag_detail);
// GET request for list of all Tag.
router.get('/tags', tag_controller.tag_list);



/// COURSE ROUTES ///

// POST request for creating Course.
router.post('/course/create', course_controller.course_create);
// POST request to delete Course.
router.delete('/course/:id', course_controller.course_delete);
// POST request to update Course.
router.post('/course/:id/update', course_controller.course_update);
// GET request for one Course.
router.get('/course/:id', course_controller.course_detail);
// GET request for list of all Course.
router.get('/courses', course_controller.course_list);



/// LESSON ROUTES ///

// POST request for creating Lesson.
router.post('/lesson/create', lesson_controller.lesson_create);
// POST request to delete Lesson.
router.delete('/lesson/:id', lesson_controller.lesson_delete);
// POST request to update Lesson.
router.post('/lesson/:id/update', lesson_controller.lesson_update);
// GET request for one Lesson.
router.get('/lesson/:id', lesson_controller.lesson_detail);
// GET request for list of all Lesson.
router.get('/lessons', lesson_controller.lesson_list);



/// SUBJECT ROUTES ///

// POST request for creating Subject.
router.post('/subject/create', subject_controller.subject_create);
// POST request to delete Subject.
router.delete('/subject/:id', subject_controller.subject_delete);
// POST request to update Subject.
router.post('/subject/:id/update', subject_controller.subject_update);
// GET request for one Subject.
router.get('/subject/:id', subject_controller.subject_detail);
// GET request for list of all Subject.
router.get('/subjects', subject_controller.subject_list);



/// VIDEO ROUTES ///

// POST request for creating Video.
router.post('/video/create', video_controller.video_create);
// POST request to delete Video.
router.delete('/video/:id/delete', video_controller.video_delete);
// POST request to update Video.
router.post('/video/:id/update', video_controller.video_update);
// GET request for one Video.
router.get('/video/:id', video_controller.video_detail);
// GET request for list of all Video.
router.get('/videos', video_controller.video_list);



module.exports = router;
