let express = require('express');
let router = express.Router();



// Require our controllers.
let video_controller = require('../controllers/videoController');
let tag_controller = require('../controllers/tagsController');
let course_controller = require('../controllers/courseController');
let subscription_controller = require('../controllers/subscriptionController');
let lesson_controller = require('../controllers/lessonController');



/// TAG ROUTES ///

// POST request for creating Tag.
router.get('/video', video_controller.analytics);
router.get('/course', course_controller.analytics);



module.exports = router;
