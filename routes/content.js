let express = require('express');
let router = express.Router();


// Require our controllers.

// Require our controllers.
let video_controller = require('../controllers/videoController');
let course_controller = require('../controllers/courseController');
let subscription_controller = require('../controllers/subscriptionController');
let subject_controller = require('../controllers/subjectController');
let lesson_controller = require('../controllers/lessonController');


/// Content ROUTES ///

router.get('/subjects', subject_controller.getActiveSubjects);
router.get('/courses', course_controller.getActiveCourses);
router.get('/course/:id', course_controller.incView, course_controller.course_detail);
router.get('/lessons', lesson_controller.getActiveLessons);
router.get('/videos', video_controller.getActiveVideos);
router.get('/video/:id', video_controller.incView, video_controller.video_detail);
router.post('/subscribe', subscription_controller.subscribe);
router.post('/unsubscribe', subscription_controller.unsubscribe);


module.exports = router;
