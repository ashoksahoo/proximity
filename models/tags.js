let mongoose = require('mongoose');

let Schema = mongoose.Schema;

let TagSchema = new Schema({
    name: {type: String, required: true, min: 1, max: 100, unique: true},
    state: {
        type: String,
        enum: ["ACTIVE", "INACTIVE", "DELETED"],
        required: true,
        default: 'ACTIVE',
    },
    createdBy : {type: Schema.ObjectId, ref: 'User'},
    lastUpdatedBy : {type: Schema.ObjectId, ref: 'User'},
}, {timestamps: true});

// Virtual for this tag instance URL.
TagSchema
    .virtual('url')
    .get(function () {
        return '/catalog/tag/' + this._id;
    });

// Export model.
module.exports = mongoose.model('Tag', TagSchema);
