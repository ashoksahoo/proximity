let mongoose = require('mongoose');

let Schema = mongoose.Schema;

let LessonSchema = new Schema({
    name: {type: String, required: true, min: 1, max: 100, unique: true},
    courses: [{type: Schema.ObjectId, ref: 'Course'}],
    state: {
        type: String,
        enum: ["ACTIVE", "INACTIVE", "DELETED"],
        required: true,
        default: 'ACTIVE',
    },
    createdBy : {type: Schema.ObjectId, ref: 'User'},
    lastUpdatedBy : {type: Schema.ObjectId, ref: 'User'},
}, {timestamps: true});

// Virtual for this lesson instance URL.
LessonSchema
    .virtual('url')
    .get(function () {
        return '/catalog/lesson/' + this._id;
    });

// Export model.
module.exports = mongoose.model('Lesson', LessonSchema);
