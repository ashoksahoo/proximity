let mongoose = require('mongoose');
let Subject = require('./subject');

let Schema = mongoose.Schema;

let CourseSchema = new Schema({
    name: {type: String, required: true, min: 1, max: 100, unique: true},
    subjects: [{type: Schema.ObjectId, ref: 'Subject'}],
    state: {
        type: String,
        enum: ["ACTIVE", "INACTIVE", "DELETED"],
        required: true,
        default: 'ACTIVE',
    },
    views : {type: Number, default: 0},
    createdBy : {type: Schema.ObjectId, ref: 'User'},
    lastUpdatedBy : {type: Schema.ObjectId, ref: 'User'},
}, {timestamps: true});

CourseSchema.index( { views: -1} )

// Virtual for this course instance URL.
CourseSchema
    .virtual('url')
    .get(function () {
        return '/catalog/course/' + this._id;
    });

// Export model.
module.exports = mongoose.model('Course', CourseSchema);
