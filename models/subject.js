let mongoose = require('mongoose');

let Schema = mongoose.Schema;

let SubjectSchema = new Schema({
    name: {type: String, required: true, min: 1, max: 100, unique: true},
    state: {
        type: String,
        enum: ["ACTIVE", "INACTIVE", "DELETED"],
        required: true,
        default: 'ACTIVE',
    },
    createdBy : {type: Schema.ObjectId, ref: 'User'},
    lastUpdatedBy : {type: Schema.ObjectId, ref: 'User'},
}, {timestamps: true});

// Virtual for this subject instance URL.
SubjectSchema
    .virtual('url')
    .get(function () {
        return '/catalog/subject/' + this._id;
    });

// Export model.
module.exports = mongoose.model('Subject', SubjectSchema);
