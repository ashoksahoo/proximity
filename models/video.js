let mongoose = require('mongoose');

let Schema = mongoose.Schema;

let VideoSchema = new Schema({
    title: {type: String, required: true},
    link: {type: String, required: true},
    tags: [{type: Schema.ObjectId, ref: 'Tag'}],
    lessons: [{type: Schema.ObjectId, ref: 'Lesson'}],
    state: {
        type: String,
        enum: ["ACTIVE", "INACTIVE", "DELETED"],
        required: true,
        default: 'ACTIVE',
    },
    views : {type: Number, default: 0},
    createdBy : {type: Schema.ObjectId, ref: 'User'},
    lastUpdatedBy : {type: Schema.ObjectId, ref: 'User'},
}, {timestamps: true});

VideoSchema.index( { views: -1} )

// Virtual for this video instance URL.
VideoSchema
    .virtual('url')
    .get(function () {
        return '/catalog/video/' + this._id;
    });

// Export model.
module.exports = mongoose.model('Video', VideoSchema);
