let mongoose = require('mongoose');

let Schema = mongoose.Schema;

let SubscriptionSchema = new Schema({
    course: {type: Schema.ObjectId, ref: 'Course'},
    state: {
        type: String,
        enum: ["ACTIVE", "INACTIVE", "DELETED"],
        required: true,
        default: 'ACTIVE',
    },
    user : {type: Schema.ObjectId, ref: 'User'},
    createdBy : {type: Schema.ObjectId, ref: 'User'},
    lastUpdatedBy : {type: Schema.ObjectId, ref: 'User'},
}, {timestamps: true});

SubscriptionSchema.index( { course: 1, user: 1}, { unique: true } )
// Export model.
module.exports = mongoose.model('Subscription', SubscriptionSchema);
