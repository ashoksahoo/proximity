let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let crypto = require('crypto');

let UserSchema = new Schema({
    name: {type: String, required: true},
    is_instructor: {type: Boolean, required: true},
    email: {type: String, required: true, unique: true},
    salt: {type: String, required: true},
    hash: {type: String, required: true},
    state: {
        type: String,
        enum: ["ACTIVE", "INACTIVE", "DELETED"],
        required: true,
        default: 'ACTIVE',
    },
    createdBy : {type: Schema.ObjectId, ref: 'User'},
    lastUpdatedBy : {type: Schema.ObjectId, ref: 'User'},
}, {timestamps: true});

// Virtual for User's URL.
UserSchema.virtual('url').get(function () {
    return '/users/' + this._id;
});

// Instance method for hashing user-typed password.
UserSchema.methods.setPassword = function (password) {
    // Create a salt for the user.
    this.salt = crypto.randomBytes(16).toString('hex');
    // Use salt to create hashed password.
    this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 128, 'sha512').toString('hex');
};

// Instance method for comparing user-typed password against hashed-password on db.
UserSchema.methods.validatePassword = function (password) {
    let hash = crypto.pbkdf2Sync(password, this.salt, 10000, 128, 'sha512').toString('hex');
    return this.hash === hash;
};

// Instance method for comparing user-typed passwords against each other.
UserSchema.methods.passwordsMatch = function (password, passwordConfirm) {
    return password === passwordConfirm;
};

// Export model
module.exports = mongoose.model('User', UserSchema);
