let express = require('express');
let path = require('path');
let favicon = require('serve-favicon');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');

let auth = require('./lib/auth');
let index = require('./routes/index');
let users = require('./routes/users');
let catalog = require('./routes/catalog'); // Import routes for 'catalog' area of site
let analytics = require('./routes/analytics'); // Import routes for 'catalog' area of site
let content = require('./routes/content'); // Import routes for 'catalog' area of site
let compression = require('compression');
let helmet = require('helmet');

// Create the Express application object
let app = express();

// Set up mongoose connection
let mongoose = require('mongoose');
let dev_db_url = 'mongodb://localhost:27017/catalog';
let mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB);
mongoose.set('debug', true);
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// Authentication Packages
let session = require('express-session');
let passport = require('passport');
let LocalStrategy = require('passport-local').Strategy;
let User = require('./models/user');
let MongoStore = require('connect-mongo')(session);

// Configure the local strategy for use by Passport.
passport.use(
    new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    }, function (email, password, callback) {
        User.findOne({email}, function (err, user) {
            if (err) {
                return callback(err);
            }
            if (!user) {
                return callback(null, false, {message: 'Incorrect Email. '});
            }
            if (!user.validatePassword(password)) {
                return callback(null, false, {message: 'Incorrect password.'});
            }
            return callback(null, user);
        });
    })
);

// Configure Passport authenticated session persistence.
passport.serializeUser(function (user, callback) {
    callback(null, user._id);
});

passport.deserializeUser(function (id, callback) {
    User.findById(id, {"id": 1, "email": 1, name: 1, is_instructor: 1}, function (err, user) {
        if (err) {
            return callback(err);
        }
        callback(null, user);
    });
});

// Uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

app.use(compression()); // Compress all routes
app.use(helmet());

app.use(express.static(path.join(__dirname, 'public')));

// Authentication related middleware.
app.use(
    session({
        secret: 'local-library-session-secret',
        resave: false,
        saveUninitialized: true,
        store: new MongoStore({
            url: mongoDB,
            ttl: 7 * 24 * 60 * 60 // 7 days. 14 Default.
        })
        // cookie: { secure: true }
    })
);

// Initialize Passport and restore authentication state, if any,
// from the session.
app.use(passport.initialize());
app.use(passport.session());

// Pass isAuthenticated and current_user to all views.
app.use(function (req, res, next) {
    res.locals.isAuthenticated = req.isAuthenticated();
    // Delete salt and hash fields from req.user object before passing it.
    let safeUser = req.user;
    if (safeUser) {
        delete safeUser._doc.salt;
        delete safeUser._doc.hash;
    }
    res.locals.current_user = safeUser;
    next();
});

// Use our Authentication and Authorization middleware.
app.use(auth);

app.use('/', index);
app.use('/users', users);
app.use('/catalog', catalog);
app.use('/content', content);
app.use('/analytics', analytics);

// Catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// Error handler
app.use(function (err, req, res, next) {
    // Set locals, only providing error in development
    let error = req.app.get('env') === 'development' ? err : {};

    // Render the error page
    res.status(err.status || 500);
    res.send({error});
});

module.exports = app;
