# Problem Statement
https://proximity-tech.github.io/be-challenge/

# Postman Collection
https://www.getpostman.com/collections/537527bdc2c1dad237e1

# Architectural overview
NodeJS API server, with MongoDB

User Apis /user/ (Only user signup is supported)
Catalog APIS /catalog/* (Managing Content for Instructor)
Content APIS /content/* (Getting Content for User)
Analytics APIS /analytics/*

#T ools Used
ExpressJs, NodeJs, MongoDB with Mongoose.

# Setup

```
npm install
node populatedb.js
node bin/www.js
```

populatedb.js creates Instreuctor user Who has crud Access on Content.
