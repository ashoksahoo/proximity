#! /usr/bin/env node
let mongoose = require('mongoose');

// Get arguments passed on command line
let Video = require('./models/video')
let Tag = require('./models/tags')
let Subject = require('./models/subject')
let Lesson = require('./models/lesson')
let Course = require('./models/course')
let User = require('./models/User')


let tags = []
let videos = []
let subjects = []
let lessons = []
let courses = []
let createdBy = {}


async function createUser() {
    let user = new User({name: "ashok", email: 'hello@ashok.io', is_instructor: true});
    user.setPassword("123456");
    createdBy = await user.save();
}


async function subjectCreate(name) {
    let s = new Subject({name, createdBy});
    let o = await s.save();
    subjects.push(o)
}

async function courseCreate(name, subjects) {
    let c = new Course({name, subjects, createdBy});
    let o = await c.save();
    courses.push(o)
}

async function lessonCreate(name, courses) {
    let l = new Lesson({name, courses, createdBy});
    let o = await l.save();
    lessons.push(o)
}

async function tagCreate(name) {
    let tag = new Tag({name, createdBy});
    let tagO = await tag.save();
    tags.push(tagO)
}


async function videoCreate(title, link, tags, lessons) {
    let videodetail = {
        title,
        link,
        tags,
        lessons,
        createdBy
    }
    let video = new Video(videodetail);
    let videoObj = await video.save();
    videos.push(videoObj)
}


async function createTags() {
    await tagCreate("LTT");
    await tagCreate("Fantasy");
    await tagCreate("Science Fiction");
    await tagCreate("French Poetry");
}


async function createSubject() {
    await subjectCreate("Math");
    await subjectCreate("Science");
    await subjectCreate("History");
}


async function createCourse() {
    await courseCreate("IIT-JEE", [subjects[0], subjects[1]]);
    await courseCreate("NEET", [subjects[0], subjects[1]]);
}


async function createLesson() {
    await lessonCreate("Trigonometry", [courses[0], courses[1]]);
    await lessonCreate("Aritimatic", [courses[0], courses[1]]);
}


async function createVideos() {
    await videoCreate('Turn your Outdated Computer into a Monitor!', 'https://www.youtube.com/watch?v=u4bGGtnc6Ds',
        tags, lessons);
}


async function doIt() {

    await mongoose.connect('mongodb://localhost:27017/catalog');
    let db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));
    db.on('connect', console.error.bind(console, 'MongoDB Connected'));
    await db.dropDatabase();
    await createUser()
    await createSubject()
    await createCourse()
    await createLesson()
    await createTags()
    await createVideos();
}


doIt().then(
    function () {


    }).catch(function (err) {
    console.log('FINAL ERR: ' + err);
}).finally(function () {
    mongoose.connection.close();
})




