let express = require('express');
let router = express.Router();

// Catch all requests pointed to 'delete' or 'update' page of our catalog entities,
// and run them through our authentication/authorization middleware chain.
// This route requires 'update' or 'delete' permission.
// e.g. /catalog/author/:id/update
router.use(/^\/catalog/, [
    confirmAuthentication,
    confirmInstructor
]);
router.use(/^\/analytics/, [
    confirmAuthentication,
    confirmInstructor
]);


// Confirms that the user is authenticated.
function confirmAuthentication(req, res, next) {
    if (req.isAuthenticated()) {
        // Authenticated. Proceed to next function in the middleware chain.
        return next();
    } else {
        // Not authenticated. Redirect to login page and flash a message.
        res.status(403);
        res.send({err: "Unauthorized"});
    }
}

// Confirms that the user has appropriate permission.
function confirmInstructor(req, res, next) {
    if (res.locals.current_user.is_instructor) {
        // User has required permission.
        return next();
    } else {
        // User does not have required permission.
        res.status(403);
        res.send({err: "Unauthorized"});
    }
}

module.exports = router;
