let {body, validationResult} = require('express-validator/check');
let {sanitizeBody} = require('express-validator/filter');
let passport = require('passport');

// Require user model
let User = require('../models/user');

// Handle login form on POST
exports.login = [
    passport.authenticate('local', {}),
    function (req, res, next) {
        res.send(res.locals.current_user)
    }

];

// Handle logout on GET.
exports.logout = [
    function (req, res, next) {
        req.logout();
        req.session.destroy(err => {
            res.send({});
        });
    }
];

// Handle register on POST.
exports.register = [
    // Validate fields.
    body('name', 'Full name must be at least 3 characters long.').isLength({min: 3}).trim(),
    body('email', 'Please enter a valid email address.').isEmail().trim(),
    body('password', 'Password must be between 4-32 characters long.').isLength({min: 4, max: 32}).trim(),

    // Sanitize fields with wildcard operator.
    sanitizeBody('*').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {
        // Extract the validation errors from a request.
        let errors = validationResult(req);

        // Get a handle on errors.array() array,
        // so we can push our own error messages into it.
        let errorsArray = errors.array();

        // Create a user object with escaped and trimmed data.
        let user = new User({
            name: req.body.name,
            email: req.body.email,
            is_instructor: false,
        });

        if (errorsArray.length > 0) {
            // There are errors. Render the form again with sanitized values/error messages.
            res.send({
                errors: errorsArray
            });
        } else {
            // Data from form is valid.

            // Passwords match. Set password.
            user.setPassword(req.body.password);

            // Check if User with same username already exists.
            User.findOne({email: req.body.email}).exec((err, found_user) => {
                if (err) {
                    return next(err);
                }
                if (found_user) {
                    // Username exists, re-render the form with error message.
                    res.send({
                        errors: [{msg: 'Email Exists. Please login'}]
                    });
                } else {
                    // User does not exist. Create it.
                    user.save(err => {
                        if (err) {
                            return next(err);
                        }
                        // User saved. Redirect to login page.
                        res.send({result: true});
                    });
                }
            });
        }
    }
];

