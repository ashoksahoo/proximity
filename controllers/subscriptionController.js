let Lesson = require('../models/lesson');
let Video = require('../models/video');
let Subscription = require('../models/subscription');

exports.subscribe = async function (req, res) {
    let user = res.locals.current_user;
    let course = req.body.course;
    let subscription = await Subscription.update({
        user,
        course,
    }, {state: "ACTIVE"}, {upsert: true, new: true})
    res.send({subscription})
}

exports.unsubscribe = async function (req, res) {
    let user = res.locals.current_user;
    let course = req.body.course;
    let subscription = await Subscription.update({
        user,
        course,
    }, {state: "INACTIVE"}, {upsert: true, new: true})
    res.send({subscription})
}
