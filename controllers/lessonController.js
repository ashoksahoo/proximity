let Subject = require('../models/subject');
let Lesson = require('../models/lesson');
let Video = require('../models/video');
let Course = require('../models/course');

const {body, validationResult, query} = require('express-validator/check');
const {sanitizeBody} = require('express-validator/filter');


// Display list of all Lesson.
exports.lesson_list = async function (req, res, next) {
    let query = {state: {$ne: "DELETED"}};
    let lessons = await Lesson.find(query)
        .sort([['name', 'ascending']])
        .populate({
            path: 'courses',
            select: 'name',
            match: {state: {$ne: 'DELETED'}}
        }).lean();
    res.send(lessons)
};

// Display detail page for a specific Lesson.
exports.lesson_detail = async function (req, res, next) {
    let lesson = await Lesson.findById(req.params.id).populate({
        path: 'courses',
        select: 'name',
        match: {state: {$ne: 'DELETED'}}
    }).lean();
    res.send({results: {lesson}});

};
// Handle Lesson create on POST.
exports.lesson_create = [

    // Validate that the name field is not empty.
    body('name', 'Lesson name required').isLength({min: 1}).trim(),
    body('courses', 'Lesson Courses Invalid').custom(async values => {
        let courses = await Course.find({
            _id: {$in: values},
            "state": "ACTIVE"
        });
        if (!courses.length) {
            return Promise.reject('Invalid subjects');
        }
        return true
    }),

    // Sanitize (trim and escape) the name field.
    sanitizeBody('name').trim().escape(),

    // Process request after validation and sanitization.
    async (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create a lesson object with escaped and trimmed data.
        let lesson = new Lesson(
            {
                name: req.body.name,
                courses: req.body.courses,
            }
        );

        if (!errors.isEmpty()) {
            // There are errors. Render the form again with sanitized values/error messages.
            res.send({errors: errors.array()});
        } else {
            // Data from form is valid.
            // Check if Lesson with same name already exists.
            let found_lesson = await Lesson.findOne({'name': req.body.name});
            if (found_lesson) {
                // Lesson exists, redirect to its detail page.
                res.redirect(found_lesson.url);
            } else {
                let t = await lesson.save()
                res.redirect(t.url);
            }
        }
    }
];

// Handle Lesson delete on POST.
exports.lesson_delete = async function (req, res, next) {

    let lesson = await Lesson.findById(req.params.id)
    let video = await Video.find({'lesson': req.params.id})
    // Success
    if (!lesson) {
        res.send({error: "Lesson Not found"});
    } else {
        //  Delete object and redirect to the list of lessons.
        await Lesson.findByIdAndUpdate(req.body.id, {state: "DELETED"});
        res.send({result: true});
    }
};

// Handle Lesson update on POST.
exports.lesson_update = [

    // Validate that the name field is not empty.
    body('name', 'Lesson name required').isLength({min: 1}).trim(),
    body('courses', 'Lesson Courses Invalid').custom(async values => {
        let courses = await Course.find({
            _id: {$in: values},
            "state": "ACTIVE"
        });
        return courses.length;

    }),
    body('state', 'Lesson State required').isIn(["ACTIVE", "INACTIVE", "DELETED"]).trim(),

    // Sanitize (trim and escape) the name field.
    sanitizeBody('name').trim().escape(),

    // Process request after validation and sanitization.
    async (req, res, next) => {

        // Extract the validation errors from a request .
        const errors = validationResult(req);

        // Create a lesson object with escaped and trimmed data (and the old id!)
        let lesson = new Lesson(
            {
                name: req.body.name,
                _id: req.params.id,
                state: req.body.state,
                courses: req.body.courses,
            }
        );


        if (!errors.isEmpty()) {
            // There are errors. Render the form again with sanitized values and error messages.
            res.send({errors: errors.array()});
        } else {
            // Data from form is valid. Update the record.
            let t = await Lesson.findByIdAndUpdate(req.params.id, lesson, {});
            res.redirect(t.url);
        }
    }
];


exports.getActiveLessons = [
    query('course', 'Invalid course').optional().isMongoId().trim(),
    async function (req, res) {
        const errors = validationResult(req);
        console.warn(errors)
        if (!errors.isEmpty()) {
            // There are errors.
            return res.send({errors: errors.array()});
        }
        let query = {state: "ACTIVE"};
        let limit = req.query.limit || 100;
        let skip = req.query.skip || 0;
        if (req.query.name) {
            query.name = req.query.name;
        }
        if (req.query.course) {
            query.courses = req.query.course;
        }
        let lesson = await Lesson.find(query, {}, {skip, limit})
            .sort([['name', 'ascending']])
            .populate({
                path: 'courses',
                select: 'name',
                match: {state: "ACTIVE"}
            }).lean();
        res.send({lesson})

    }]
