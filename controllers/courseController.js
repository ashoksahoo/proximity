let Course = require('../models/course');
let Subject = require('../models/subject');
let Video = require('../models/video');

const {body, validationResult, query} = require('express-validator/check');
const {sanitizeBody} = require('express-validator/filter');


// Display list of all Course.
exports.course_list = async function (req, res, next) {
    let query = {state: {$ne: "DELETED"}};
    let courses = await Course.find(query, {"views": 0})
        .sort([['name', 'ascending']])
        .populate({
            path: 'subjects',
            select: 'name',
            match: {state: {$ne: 'DELETED'}}
        }).lean();
    res.send(courses)
};

// Display detail page for a specific Course.
exports.course_detail = async function (req, res, next) {
    let course = await Course.findById(req.params.id, {"views": 0}).populate({
        path: 'subjects',
        select: 'name',
        match: {state: {$ne: 'DELETED'}}
    }).lean();
    res.send({results: {course}});

};

// Handle Course create on POST.
exports.course_create = [

    // Validate that the name field is not empty.
    body('name', 'Course name required').isLength({min: 1}).trim(),
    body('subjects', 'Course Subjects Invalid').custom(async values => {
        let subjects = await Subject.find({
            _id: {$in: values},
            "state": "ACTIVE"
        });
        if (!subjects.length) {
            return Promise.reject('Invalid subjects');
        }
        return true
    }),

    // Sanitize (trim and escape) the name field.
    sanitizeBody('name').trim().escape(),

    // Process request after validation and sanitization.
    async (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create a course object with escaped and trimmed data.
        let course = new Course(
            {
                name: req.body.name,
                subjects: req.body.subjects,
            }
        );


        if (!errors.isEmpty()) {
            // There are errors. Render the form again with sanitized values/error messages.
            res.send({errors: errors.array()});
        } else {
            // Data from form is valid.
            // Check if Course with same name already exists.
            let found_course = await Course.findOne({'name': req.body.name});
            if (found_course) {
                // Course exists, redirect to its detail page.
                res.redirect(found_course.url);
            } else {
                let c = await course.save()
                // Course saved. Redirect to course detail page.
                res.redirect(c.url);
            }
        }
    }
];

// Handle Course delete on POST.
exports.course_delete = async function (req, res, next) {

    let course = await Course.findById(req.params.id)
    // Success
    if (!course) {
        res.send({error: "Course Not found"});
    } else {
        //  Delete object and redirect to the list of courses.
        Course.findByIdAndUpdate(req.body.id, {state: "DELETED"}, function deleteCourse(err) {
            if (err) {
                return next(err);
            }
            // Success - go to courses list.
            res.send({result: true});
        });

    }
};

// Handle Course update on POST.
exports.course_update = [

    // Validate that the name field is not empty.
    body('name', 'Course name required').isLength({min: 1}).trim(),
    body('subjects', 'Course Subjects Invalid').custom(async values => {
        let subjects = await Subject.find({
            _id: {$in: values},
            "state": "ACTIVE"
        });
        if (!subjects.length) {
            return Promise.reject('Invalid subjects');
        }
        return true
    }),
    body('state', 'Course State required').isIn(["ACTIVE", "INACTIVE", "DELETED"]).trim(),

    // Sanitize (trim and escape) the name field.
    sanitizeBody('name').trim().escape(),

    // Process request after validation and sanitization.
    async (req, res, next) => {

        // Extract the validation errors from a request .
        const errors = validationResult(req);

        // Create a course object with escaped and trimmed data (and the old id!)
        let course = new Course(
            {
                name: req.body.name,
                _id: req.params.id,
                state: req.body.state,
                subjects: req.body.subjects,
            }
        );


        if (!errors.isEmpty()) {
            // There are errors. Render the form again with sanitized values and error messages.
            res.send({errors: errors.array()});
        } else {
            // Data from form is valid. Update the record.
            let c = await Course.findByIdAndUpdate(req.params.id, course, {});
            res.redirect(c.url);
        }
    }
];

exports.getActiveCourses = [
    query('subject', 'Invalid subject').optional().isMongoId().trim(),
    async function (req, res) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            // There are errors.
            return res.send({errors: errors.array()});
        }
        let query = {state: "ACTIVE"};
        let limit = req.query.limit || 100;
        let skip = req.query.skip || 0;
        if (req.query.name) {
            query.name = req.query.name;
        }
        if (req.query.subject) {
            query.subjects = req.query.subject;
        }
        let courses = await Course.find(query, {"views": 0}, {skip, limit})
            .sort([['name', 'ascending']])
            .populate({
                path: 'subjects',
                select: 'name',
                match: {state: "ACTIVE"}
            }).lean();
        res.send({courses})
    }]

exports.incView = async function (req, res, next) {
    await Course.update({_id: req.params.id}, {$inc: {views: 1}})
    next();
}

exports.analytics = async function (req, res, next) {
    let analytics = await Course.find({}, {name: 1, views: 1}, {limit: 10})
        .sort([['views', 'descending']])
    res.send({analytics})
}

