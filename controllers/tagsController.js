let Course = require('../models/course');
let Subject = require('../models/subject');
let Lesson = require('../models/lesson');
let Video = require('../models/video');
let Tag = require('../models/tags');

const {body, validationResult} = require('express-validator/check');
const {sanitizeBody} = require('express-validator/filter');


// Display list of all Tag.
exports.tag_list = async function (req, res, next) {
    let query = {state: {$ne: "DELETED"}};
    let tags = await Tag.find(query)
        .sort([['name', 'ascending']])
    res.send(tags)
};

// Display detail page for a specific Tag.
exports.tag_detail = async function (req, res, next) {
    let tag = await Tag.findById(req.params.id)
    let videos = await Video.find({'tag': req.params.id});
    res.send({results: {tag, videos}});

};

// Handle Tag create on POST.
exports.tag_create = [

    // Validate that the name field is not empty.
    body('name', 'Tag name required').isLength({min: 1}).trim(),

    // Sanitize (trim and escape) the name field.
    sanitizeBody('name').trim().escape(),

    // Process request after validation and sanitization.
    async (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create a tag object with escaped and trimmed data.
        let tag = new Tag(
            {name: req.body.name}
        );


        if (!errors.isEmpty()) {
            // There are errors. Render the form again with sanitized values/error messages.
            res.send({errors: errors.array()});
        } else {
            // Data from form is valid.
            // Check if Tag with same name already exists.
            let found_tag = await Tag.findOne({'name': req.body.name});
            if (found_tag) {
                // Tag exists, redirect to its detail page.
                res.redirect(found_tag.url);
            } else {
                let t = await tag.save()
                res.redirect(t.url);
            }
        }
    }
];

// Handle Tag delete on POST.
exports.tag_delete = async function (req, res, next) {

    let tag = await Tag.findById(req.params.id)
    let video = await Video.find({'tag': req.params.id})
    // Success
    if (!tag) {
        res.send({error: "Tag Not found"});
    }
    if (video.length > 0) {
        res.send({error: "Tag has Videos"});
    } else {
        //  Delete object and redirect to the list of tags.
        await Tag.findByIdAndUpdate(req.body.id, {state: "DELETED"});
        res.send({result: true});
    }
};

// Handle Tag update on POST.
exports.tag_update = [

    // Validate that the name field is not empty.
    body('name', 'Tag name required').isLength({min: 1}).trim(),
    body('state', 'Tag State required').isIn(["ACTIVE", "INACTIVE", "DELETED"]).trim(),

    // Sanitize (trim and escape) the name field.
    sanitizeBody('name').trim().escape(),

    // Process request after validation and sanitization.
    async (req, res, next) => {

        // Extract the validation errors from a request .
        const errors = validationResult(req);

        // Create a tag object with escaped and trimmed data (and the old id!)
        let tag = new Tag(
            {
                name: req.body.name,
                _id: req.params.id
            }
        );


        if (!errors.isEmpty()) {
            // There are errors. Render the form again with sanitized values and error messages.
            res.send({errors: errors.array()});
        } else {
            // Data from form is valid. Update the record.
            let t = await Tag.findByIdAndUpdate(req.params.id, tag, {});
            res.redirect(t.url);
        }
    }
];
