let Lesson = require('../models/lesson');
let Video = require('../models/video');
let Tag = require('../models/tags');

const {body, validationResult, query} = require('express-validator/check');
const {sanitizeBody} = require('express-validator/filter');

// Display list of all videos.
exports.video_list = async function (req, res, next) {
    let query = {state: {$ne: "DELETED"}};
    let lessons = await Video.find(query, {"views": 0})
        .sort([['name', 'ascending']])
        .populate({
            path: 'tags',
            select: 'name',
            match: {state: {$ne: 'DELETED'}}
        }).populate({
            path: 'lessons',
            select: 'name',
            match: {state: {$ne: 'DELETED'}}
        }).lean();
    res.send(lessons)

};

// Display detail page for a specific video.
exports.video_detail = async function (req, res, next) {

    let video = await Video.findById(req.params.id, {"views": 0})
        .populate({
            path: 'tags',
            select: 'name',
            match: {state: {$ne: 'DELETED'}}
        }).populate({
            path: 'lessons',
            select: 'name',
            match: {state: {$ne: 'DELETED'}}
        }).lean();
    if (!video) { // No results.
        let err = new Error('Video not found');
        err.status = 404;
        return next(err);
    }
    res.send({results: video});

};

// Handle video create on POST.
exports.video_create = [
    // Convert the tag to an array.
    (req, res, next) => {
        if (!(req.body.tags instanceof Array)) {
            if (typeof req.body.tags === 'undefined')
                req.body.tags = [];
            else
                req.body.tags = new Array(req.body.tags);
        }
        next();
    },

    // Validate fields.
    body('title', 'Title must not be empty.').isLength({min: 1}).trim(),
    body('link', 'Link must be a valid URL.').isURL().trim(),
    body('lessons', 'Video lessons Invalid').custom(async values => {
        let lessons = await Lesson.find({
            _id: {$in: values},
            "state": "ACTIVE"
        });
        return lessons.length;
    }),
    // Sanitize fields.
    sanitizeBody('title').trim().escape(),
    // Process request after validation and sanitization.
    async (req, res, next) => {
        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create a Video object with escaped and trimmed data.
        let video = new Video(
            {
                title: req.body.title,
                link: req.body.link,
                lessons: req.body.lessons,
                tags: req.body.tags
            });

        if (!errors.isEmpty()) {
            res.send({errors: errors.array()});
        } else {
            // Data from form is valid.
            let v = await video.save()
            res.redirect(v.url);
        }
    }
]
;

// Handle video delete on POST.
exports.video_delete = async function (req, res, next) {

    let video = await Video.findById(req.params.id)
    // Success
    if (!video) {
        res.send({error: "Video Not found"});
    } else {
        //  Delete object and redirect to the list of lessons.
        await Video.findByIdAndUpdate(req.body.id, {state: "DELETED"});
        res.send({result: true});
    }

};


// Handle video update on POST.
exports.video_update = [

    // Convert the tag to an array.
    (req, res, next) => {
        if (!(req.body.tags instanceof Array)) {
            if (typeof req.body.tags === 'undefined')
                req.body.tags = [];
            else
                req.body.tags = new Array(req.body.tags);
        }
        next();
    },

    // Validate fields.
    body('title', 'Title must not be empty.').isLength({min: 1}).trim(),
    body('url', 'URL must not be empty.').isURL().trim(),
    body('lessons', 'Video lessons Invalid').custom(async values => {
        let lessons = await Lesson.find({
            _id: {$in: values},
            "state": "ACTIVE"
        });
        return lessons.length;
    }),
    body('state', 'Video State required').isIn(["ACTIVE", "INACTIVE", "DELETED"]).trim(),
    // Sanitize fields.
    sanitizeBody('*').trim().escape(),
    sanitizeBody('tag.*').trim().escape(),

    // Process request after validation and sanitization.
    async (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create a Video object with escaped/trimmed data and old id.
        let video = new Video(
            {
                title: req.body.title,
                url: req.body.url,
                lessons: req.body.lessons,
                tags: req.body.tags,
                _id: req.params.id // This is required, or a new ID will be assigned!
            });

        if (!errors.isEmpty()) {
            // There are errors. Render the form again with sanitized values and error messages.
            res.send({errors: errors.array()});
        } else {
            // Data from form is valid. Update the record.
            let t = await Video.findByIdAndUpdate(req.params.id, video, {});
            res.redirect(t.url);
        }
    }
];

exports.getActiveVideos = [
    query('lesson', 'Invalid lesson').optional().isMongoId().trim(),
    async function (req, res) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            // There are errors.
            return res.send({errors: errors.array()});
        }
        let query = {state: "ACTIVE"};
        let limit = req.query.limit || 100;
        let skip = req.query.skip || 0;
        if (req.query.title) {
            query.title = req.query.title;
        }
        if (req.query.lesson) {
            query.lessons = req.query.lesson;
        }
        if (req.query.tag) {
            query.tags = req.query.tag;
        }
        let videos = await Video.find(query, {"views": 0}, {skip, limit})
            .sort([['name', 'ascending']])
            .populate({
                path: 'lessons',
                select: 'name',
                match: {state: "ACTIVE"}
            }).populate({
                path: 'tags',
                select: 'name',
                match: {state: "ACTIVE"}
            }).lean();
        res.send({videos})
    }]

exports.incView = async function (req, res, next) {
    await Video.update({_id: req.params.id}, {$inc: {views: 1}})
    next();
}

exports.analytics = async function (req, res, next) {
    let analytics = await Video.find({}, {name: 1, views: 1}, {limit: 10})
        .sort([['views', 'descending']])
    res.send({analytics})
}
