let Subject = require('../models/subject');
let Lesson = require('../models/lesson');
let Video = require('../models/video');

const {body, validationResult} = require('express-validator/check');
const {sanitizeBody} = require('express-validator/filter');


// Display list of all Subject.
exports.subject_list = async function (req, res, next) {
    let query = {state: {$ne: "DELETED"}};
    let subjects = await Subject.find(query)
        .sort([['name', 'ascending']])
    res.send(subjects)
};

// Display detail page for a specific Subject.
exports.subject_detail = async function (req, res, next) {
    let subject = await Subject.findById(req.params.id)
    let lessons = await Lesson.find({'subjects': req.params.id});
    res.send({results: {subject, lessons}});

};
// Handle Subject create on POST.
exports.subject_create = [

    // Validate that the name field is not empty.
    body('name', 'Subject name required').isLength({min: 1}).trim(),

    // Sanitize (trim and escape) the name field.
    sanitizeBody('name').trim().escape(),

    // Process request after validation and sanitization.
    async (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create a subject object with escaped and trimmed data.
        let subject = new Subject(
            {name: req.body.name}
        );

        if (!errors.isEmpty()) {
            // There are errors. Render the form again with sanitized values/error messages.
            res.send({errors: errors.array()});
        } else {
            // Data from form is valid.
            // Check if Subject with same name already exists.
            let found_subject = await Subject.findOne({'name': req.body.name});
            if (found_subject) {
                // Subject exists, redirect to its detail page.
                res.redirect(found_subject.url);
            } else {
                let t = await subject.save()
                res.redirect(t.url);
            }
        }
    }
];

// Handle Subject delete on POST.
exports.subject_delete = async function (req, res, next) {

    let subject = await Subject.findById(req.params.id)
    let video = await Video.find({'subject': req.params.id})
    // Success
    if (!subject) {
        res.send({error: "Subject Not found"});
    }
    else {
        //  Delete object and redirect to the list of subjects.
        await Subject.findByIdAndUpdate(req.body.id, {state: "DELETED"});
        res.send({result: true});
    }
};

// Handle Subject update on POST.
exports.subject_update = [

    // Validate that the name field is not empty.
    body('name', 'Subject name required').isLength({min: 1}).trim(),
    body('state', 'Subject State required').isIn(["ACTIVE", "INACTIVE", "DELETED"]).trim(),

    // Sanitize (trim and escape) the name field.
    sanitizeBody('name').trim().escape(),

    // Process request after validation and sanitization.
    async (req, res, next) => {

        // Extract the validation errors from a request .
        const errors = validationResult(req);

        // Create a subject object with escaped and trimmed data (and the old id!)
        let subject = new Subject(
            {
                name: req.body.name,
                state: req.body.state,
                _id: req.params.id
            }
        );


        if (!errors.isEmpty()) {
            // There are errors. Render the form again with sanitized values and error messages.
            res.send({errors: errors.array()});
        } else {
            // Data from form is valid. Update the record.
            let t = await Subject.findByIdAndUpdate(req.params.id, subject, {});
            res.redirect(t.url);
        }
    }
];


exports.getActiveSubjects = async function (req, res) {
    let query = {state: "ACTIVE"};
    let limit = req.query.limit || 100;
    let skip = req.query.skip || 0;
    if(req.query.name){
        query.name = req.query.name;
    }
    let subjects = await Subject.find(query, {}, {skip, limit})
        .sort([['name', 'ascending']])
        .lean();
    res.send({subjects})
}

